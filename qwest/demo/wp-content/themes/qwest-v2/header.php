<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Exemple de thème WordPress</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="<?php echo bloginfo('template_directory'); ?>/assets/css/fonts.css" rel="stylesheet">
    <link href="<?php echo bloginfo('template_directory'); ?>/assets/css/variable.css" rel="stylesheet">
    <link href="<?php echo bloginfo('template_directory'); ?>/assets/css/main-styles.css" rel="stylesheet">
    <!-- HTML5 shim et Respond.js pour supporter les éléments HTML5 pour les versions plus anciennes que Internet Explorer 9 -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
            <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Menu
                        </a>
                        <div class="dropdown-menu main-menu" aria-labelledby="navbarDropdown">
                            <section class="container ml-auto">
                                <div class="row">

                                    <section class="col-3">
                                        <h1>Magazine</h1>
                                        <ul class="list">
                                            <li class="list-item"><a>Rencontres</a></li>
                                            <li class="list-item"><a>Chroniques</a></li>
                                            <li class="list-item"><a>Top du mois</a></li>
                                            <li class="list-item"><a>Grands Formats</a></li>
                                            <li class="list-item"><a>Documentaires</a></li>
                                            <li class="list-item"><a>Archives</a></li>
                                        </ul>
                                    </section>

                                    <section class="col-3">
                                        <h1>Chroniques</h1>
                                        <ul class="list">
                                            <li class="list-item"><a>Les plus récentes</a></li>
                                            <li class="list-item"><a>Toutes les chroniques<a></li>
                                        </ul>
                                    </section>

                                    <section class="col-3">
                                        <h1>Agenda</h1>
                                        <ul class="list">
                                            <li class="list-item"><a>A venir</a></li>
                                            <li class="list-item"><a>Dans ma ville</a></li>
                                            <li class="list-item"><a>Concerts</a></li>
                                        </ul>
                                    </section>

                                    <div class="col-2 post-moment" style="position: relative">
                                        <div style="position: absolute; width: 34vw">
                                            <h1>En ce moment sur Qwest TV</h1>
                                            <img src="https://qwest.tv/wp-content/uploads/2019/02/manuuuuu-800x533.jpg" alt="Manu Dibango">
                                        </div>
                                    </div>

                                </div>
                            </section>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="navbar-header">
                <a class="navbar-brand navbar-brand-center" href="<?php $_SERVER['HTTP_HOST'] ?>">QwestTV</a>
            </div>

            <div class="nav navbar-nav ml-auto">
                <a href="https://video.qwest.tv" class="nav-item">Discover QwestTV</a>
            </div>
        </div>
    </nav>
</header>